#!/bin/sh
#######
## LineageOS-Setup.sh
#######


https://github.com/LineageOS/lineage_wiki
https://wiki.lineageos.org/devices/x2

https://download.lineageos.org/x2
https://download.lineageos.org/extras

https://wiki.lineageos.org/devices/x2/install
https://wiki.lineageos.org/verifying-builds.html
https://github.com/lineageos/lineage_wiki/blob/master/pages/meta/verifying_builds.md

## Verifying Build Authenticity
## All official builds from LineageOS are signed with our private keys. 
## You can verify a build has been signed with our keys by running:

keytool -list -printcert -jarfile lineage-build-signed.zip
keytool -J-Duser.language=en -list -printcert -jarfile lineage-build-signed.zip

## The resulting fingerprints are listed as follows:
Certificate fingerprints:
         MD5:  F2:CA:AA:A7:2F:D6:34:FE:70:D7:5C:41:43:6C:5E:14
         SHA1: 9B:6D:F9:06:2A:1A:76:E6:E0:07:B1:1F:C2:EF:CB:EF:4B:32:F2:23
         SHA256: 51:83:25:EF:7F:96:C0:D1:19:4C:2E:85:6B:04:0D:63:61:66:FF:B8:46:71:7D:72:FA:87:F4:FA:E5:BE:7B:BB





https://magiskmanager.com/downloading-magisk-manager
https://github.com/topjohnwu/Magisk/releases
https://github.com/topjohnwu/MagiskManager/releases


https://github.com/Magisk-Modules-Repo
https://github.com/Magisk-Modules-Repo/Fdroid-Priv
https://github.com/Magisk-Modules-Repo/Busybox-Installer
https://github.com/Magisk-Modules-Repo/YalpstorePriv
https://github.com/Magisk-Modules-Repo/KaliNethunter
https://github.com/Magisk-Modules-Repo/Xposed-Framework-Unity
https://github.com/Magisk-Modules-Repo/SSH-for-Magisk
https://github.com/Magisk-Modules-Repo/Unified-Hosts-Adblock
https://github.com/Magisk-Modules-Repo/dnscrypt-proxy-magisk
https://github.com/Magisk-Modules-Repo/magisk_selinux_manager
https://github.com/Magisk-Modules-Repo/xposed_27
https://github.com/Magisk-Modules-Repo/Init.d-Injector
https://github.com/Magisk-Modules-Repo/Systemless-keweon-DNS-Manager
https://github.com/Magisk-Modules-Repo/crontab



https://twrp.me/leeco/leecolemax2.html

https://dl.twrp.me/twrpapp/
https://dl.twrp.me/x2/
https://dl.twrp.me/x2/twrp-3.2.2-0-x2.img.html


https://twrp.me/faq/pgpkeys.html
https://dl.twrp.me/public.asc

gpg --import twrp-public.asc
gpg --verify twrp-device-version.type.asc twrp-device-version.type


https://f-droid.org/repo
https://f-droid.org/archive

https://f-droid.org/wiki/page/Known_Repositories
https://f-droid.org/en/docs/Release_Channels_and_Signing_Keys/

https://f-droid.org/F-Droid.apk
https://f-droid.org/FDroid.apk.asc

43238d512c1e5eb2d6569f4a3afbf5523418b82e0a3ed1552770abb9a9c9ccab
37D2 C987 89D8 3119 4839  4E3E 41E7 044E 1DBA 2E89

Primary key fingerprint: 37D2 C987 89D8 3119 4839 4E3E 41E7 044E 1DBA 2E89
Subkey fingerprint: 802A 9799 0161 1234 6E1F EFF4 7A02 9E54 DD5D CE7A

https://gitlab.com/fdroid/fdroidclient/blob/master/f-droid.org-signing-key.gpg



https://f-droid.i2p.io/archive
https://f-droid.i2p.io/repo


https://guardianproject.info/fdroid/repo
https://s3.amazonaws.com/guardianproject/fdroid/repo (AWS mirror)
http://bdf2wcxujkg6qqff.onion/fdroid/repo (Tor Onion Service)

https://guardianproject.info/fdroid/archive
https://s3.amazonaws.com/guardianproject/fdroid/archive (AWS mirror)
http://bdf2wcxujkg6qqff.onion/fdroid/archive (Tor Onion Service)

Signing keys: https://guardianproject.info/home/signing-keys



https://gitlab.com/fdroid/privileged-extension
https://f-droid.org/packages/org.fdroid.fdroid.privileged.ota/


https://github.com/CHEF-KOCH/Online-Privacy-Test-Resource-List
https://github.com/CHEF-KOCH/Android-Privacy-Data-Protection-Tools-Mega-Thread
https://github.com/CHEF-KOCH/WebRTC-tracking

https://www.iblocklist.com/lists.php
https://github.com/CHEF-KOCH/Global-filter-list
https://github.com/notracking/hosts-blocklists#sources
https://github.com/ukanth/afwall/wiki/Advertisements-blocking#example-lists
https://github.com/CHEF-KOCH/CKs-FilterList#optional-lists


https://github.com/ukanth/afwall/wiki/CustomScripts

